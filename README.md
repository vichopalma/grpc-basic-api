# Basic API with gRPC and Protobuf (GoLang) 

### Install protoc binaries

Download & Install

Protocol Buffers compiler: https://github.com/protocolbuffers/protobuf/releases/

This zip file Will have a **_bin_** and a **_include_** folder.


-	Move the content of the **_bin_** folder to your %GOPATH%\bin directory


-	Place the content of the **_include_** directory to your **%GOPATH%\src** directory to ensure that protoc can find files. These files are called “well known types”.
Now try tiping “**protoc**” in your terminal.


Install Packages in project folder:

`go get -u -v google.golang.org/grpc `
(main grpc package)


`go get -u -v github.com/golang/protobuf/protoc-gen-go `(protocol buffers)

project structure:
* Project/
	* client
	* proto
	* server


to generate the services:
- go to service dir (in this case, `gRPC_Basic-Api/protos/service.proto` )

```
protoc --proto_path=protos --go_out=plugins=grpc:protos service.proto
```

### server http for go (apply in client folder, main file):

`go get -u github.com/gin-gonic/gin`
